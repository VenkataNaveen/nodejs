export interface CreateVandorInput {
    name: string;
    owner: string;
    foodType: [string];
    pincode: string;
    address: string;
    phone: string;
    email:string;
    password: string;
}

export interface VandorLoginInput {
    email:string;
    password:string;
}

export interface VandorPayload {
    _id:string;
    name:string;
    email:string;
    foodType:[string];
}

export interface VandorUpdateInput {
    foodType: [string];
    name: string;
    owner: string;
    phone: string;
}

export interface CreateOfferInputs {
    offerType: string;
    vendors: [any];
    title: string;
    description: string;
    minValue: number;
    offerAmount: number;
    startValidity: Date;
    endValidity: Date;
    promocode: string;
    promoType: string;
    bank: [any];
    bins: [any];
    pincode: string;
    isActive: boolean;
}