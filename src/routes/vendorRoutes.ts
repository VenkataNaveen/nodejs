import express, {Request, Response , NextFunction} from 'express';
import path from 'path';
import multer from 'multer';
import { Authenticate } from '../middlewares';
import { AddOffer, EditOffer, GetCurrentOrders, GetFoods, GetOffers, GetOrderDetails, GetProfile, ProcessOrder, UpdateProfile, UpdateProfileService, UpdateVendorCoverImage, VanderLogin, addFood } from '../controllers';

const router = express.Router();

const imageStorage = multer.diskStorage({
    destination: function(req,file, cb){
        cb(null, path.join(__dirname, '../images'))
    },
    filename: function(req,file,cb){
        cb(null, new Date().toISOString() +'_'+ file.originalname);
    }
})


const images = multer({ storage: imageStorage}).array('images', 10);

console.log(images);
// vendor login
router.post('/login',VanderLogin)

// get vendor profile with authentication, update, images of the vendor shop ,service available
router.use(Authenticate);
router.get('/profile',GetProfile)
router.patch('/profile',UpdateProfile)
router.patch('/coverimage', images,UpdateVendorCoverImage);
router.patch('/service', UpdateProfileService);

// add food in the service
router.post('/food',addFood)
// get Food
router.get('/food',GetFoods)

router.get('/orders', GetCurrentOrders);
router.put('/order/:id/process', ProcessOrder);
router.get('/order/:id', GetOrderDetails)


router.get('/offers', GetOffers);
router.post('/offer', AddOffer);
router.put('/offer/:id', EditOffer)

export {router as VendorRoute};