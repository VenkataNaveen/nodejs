import express, { Request, Response, NextFunction } from 'express';
import { CreateOrder, CreatePayment, CustomerLogin, CustomerSignUp, CustomerVerify, EditCustomerProfile, GetCustomerProfile,  RequestOtp, VerifyOffer, addToCart,  deleteCart, getCart, getOrderById, getOrders } from '../controllers';
import { Authenticate } from '../middlewares';
// import { Offer } from '../models/Offer';

const router = express.Router();

/* ------------------- Suignup / Create Customer --------------------- */
router.post('/signup', CustomerSignUp)

/* ------------------- Login --------------------- */
router.post('/login', CustomerLogin)

/* ------------------- Authentication --------------------- */
router.use(Authenticate);

/* ------------------- Verify Customer Account --------------------- */
router.patch('/verify', CustomerVerify)


/* ------------------- OTP / request OTP --------------------- */
router.get('/otp', RequestOtp)

/* ------------------- Profile --------------------- */
router.get('/profile', GetCustomerProfile)
router.patch('/profile', EditCustomerProfile)

router.post('/addToCart',addToCart);
router.get('/getCart',getCart);
router.get('/deleteCart',deleteCart)

//orders
router.post('/createorder',CreateOrder);
router.get('/getorder/:id',getOrderById);
router.get('/orders',getOrders)



router.get('/offer/verify/:id', VerifyOffer);

router.post('/create-payment', CreatePayment);



export { router as CustomerRoute}