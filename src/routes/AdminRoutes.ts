import express from 'express';
import { CreateVandor, getVandorById, getVandors } from '../controllers';

const router = express.Router();

// Create a Vandor 
router.post('/vendor', CreateVandor)
// get all Vendors
router.get('/vendors', getVandors)
// get Vandor by id
router.get('/vendor/:id', getVandorById)

export {router as AdminRoute};