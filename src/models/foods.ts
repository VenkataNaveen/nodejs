import mongoose, {Document,Schema,Model, model} from "mongoose";

 export interface FoodDoc extends Document {
    vendorId: string;
    name: string;
    description: string;
    category: string;
    foodType: string;
    readyTime: number;
    price: number;
    images:[string];
 }


 const FoodSchema = new Schema({
    vendorId: {type:String,require:true},
    name: {type:String,require:true},
    description: {type:String,require:true},
    category: {type:String},
    foodType: {type:String,require:true},
    readyTime: {type:Number},
    price:{type:Number},
    images:{type:[String]}
 },{
    toJSON: {
        transform(doc,ret) {
            delete ret.__v,
            delete ret.createdAt,
            delete ret.updatedAt
        }
    },
    timestamps:true
 });

 export const Food =model<FoodDoc>('food', FoodSchema);