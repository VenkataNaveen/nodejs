import mongoose, { ConnectOptions } from 'mongoose'; 
import { MongoUri } from '../config';


export default async() => {

    try {
        await mongoose.connect(MongoUri, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        } as ConnectOptions).then(()=>{
            console.log("DB Connected");
        })
    } catch (err) {
        console.log(err);
        process.exit(1);
    }

}
  