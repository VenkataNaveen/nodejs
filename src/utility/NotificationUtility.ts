/* ------------------- Email --------------------- */

/* ------------------- Notification --------------------- */

/* ------------------- OTP --------------------- */

export const GenerateOtp = () => {

    const otp = Math.floor(10000 + Math.random() * 900000);
    let expiry = new Date()
    expiry.setTime(new Date().getTime() + (30 * 60 * 1000));

    return {otp, expiry};
}

export const onRequestOTP = async(otp: number, toPhoneNumber: string) => {

    try {
        const accountSid = "ACbff425bdf24be126cb2f8bc08505732b";
        const authToken = "1c5d9ac17bc432d8fd1883c2276917c2";
        const client = require('twilio')(accountSid, authToken);
          console.log("client",client.messages);
        const response = await client.messages.create({
            body: `Your OTP is ${otp}`,
            from: '+13392040401',
            to: `+91-${toPhoneNumber}` // recipient phone number // Add country before the number
        });
        console.log("re",response);
        return response;
    } catch (error){
        console.log(error);
        return false
    }
    
}

/* ------------------- Payment --------------------- */