import express from 'express';
import App from './services/ExpressApp';
import { PORT } from './config';
import dbConnection from './services/DatabaseApp';

const StartServer = async () => {

    const app = express();

    await dbConnection()

    await App(app);

    app.listen(PORT, () => {
        console.log(`Listening to port 8000 ${PORT}`);
    })
}

StartServer();