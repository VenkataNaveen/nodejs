import { Request, Response, NextFunction } from "express";
import { CreateFoodInput, CreateOfferInputs, VandorLoginInput, VandorUpdateInput } from "../dto";
import { findVandor } from "./AdminController";
import { GenerateSignature, ValidatePassword } from "../utility";
import { Food } from "../models/foods";
import { Order, Vandor } from "../models";
import { Offer } from "../models/offer";


export const VanderLogin = async(req: Request, res:Response, next:NextFunction) =>{
 
 const {email,password} =<VandorLoginInput>req.body;

 const existingVandor = await findVandor("",email);
 console.log(existingVandor);
 if (existingVandor!=null) {

    const validateUser = await ValidatePassword(password,existingVandor.password,existingVandor.salt);
    console.log(validateUser);
    if(!validateUser){
        return res.json({"message":"invalid Credentials"});
    }

    const signature = GenerateSignature({
        _id: existingVandor._id,
        email:existingVandor.email,
        foodType:existingVandor.foodType,
        name:existingVandor.name
    })

    return  res.json(signature);

 }
  return res.json({message:"No Vandor found"});
}

export const GetProfile = async(req: Request, res:Response, next:NextFunction) =>{
 
    const user = req.user;
     if (user) {
      const existingUser = await findVandor(user._id);
      if(existingUser!=null) {
       return res.json(existingUser);
      }
    }
     return res.json({message:"not a valid Vandor"});

   
}


export const UpdateProfile = async(req: Request, res:Response, next:NextFunction) =>{
      const {name,owner,foodType,phone} = <VandorUpdateInput>req.body
    const user = req.user;
     if (user) {
      const existingUser = await findVandor(user._id);
      if(existingUser!=null) {
        existingUser.name=name;
        existingUser.owner=owner;
        existingUser.foodType = foodType;
        existingUser.phone = phone;
        const savedUser = await existingUser.save();
        return res.json(savedUser);
      }
    }
     return res.json({message:"not a valid Vandor"});

   
}

export const UpdateProfileService = async(req: Request, res:Response, next:NextFunction) =>{
 
    const user = req.user;
    const { lat, lng} = req.body;
     if (user) {
      const existingUser = await findVandor(user._id);
      if(existingUser!=null) {
        existingUser.serviceAvailable=!existingUser.serviceAvailable;
        if(lat && lng){
          existingUser.lat = lat;
          existingUser.lng = lng;
      }
        const savedUser = await existingUser.save();
        return res.json(savedUser);
      }
    }
     return res.json({message:"not a valid Vandor"});

   
}

export const UpdateVendorCoverImage = async (req: Request,res: Response, next: NextFunction) => {

  const user = req.user;

   if(user){

     const vendor = await findVandor(user._id);

     if(vendor !== null){

          const files = req.files as [Express.Multer.File];
        console.log(files);
          const images = files.map((file: Express.Multer.File) => file.filename);

          vendor.coverImages.push(...images);

          const saveResult = await vendor.save();
          
          return res.json(saveResult);
     }
    }
    return res.json({'message': 'Unable to Update vendor profile '})

}



export const addFood = async(req: Request, res:Response, next:NextFunction) =>{
  const { name, description, category, foodType, readyTime, price } = <CreateFoodInput>req.body;
  const user = req.user;
  console.log(req.body);
   if (user) {
    const existingUser = await findVandor(user._id);
    if(existingUser!=null) {
      const files = req.files as [Express.Multer.File];
   const images:any =[];
     // const images = files.map((file: Express.Multer.File) => file.filename);
      const food= await Food.create({
        vendorId: user._id,
        name: name,
        description: description,
        category: category,
        price: price,
        rating: 0,
        readyTime: readyTime,
        foodType: foodType,
        images: images
      })
      console.log(food);
      existingUser.foods.push(food);
      const result = (await existingUser.save());
      const vendor= await Vandor.findById(user._id).populate('foods');
      return res.json(vendor);
    }
    
  }
   return res.json({message:"not a valid Vandor"});

 
}

export const GetFoods = async (req: Request, res: Response, next: NextFunction) => {
  const user = req.user;

  if(user){

     const foods = await Food.find({ vendorId: user._id});

     if(foods !== null){
          return res.json(foods);
     }

  }
  return res.json({'message': 'Foods not found!'})
}


export const GetCurrentOrders = async (req: Request, res: Response, next: NextFunction) => {

  const user = req.user;
  
  if(user){

      const orders = await Order.find({ vendorId: user._id}).populate('items.food');

      if(orders != null){
          return res.status(200).json(orders);
      }
  }

  return res.json({ message: 'Orders Not found'});
}

export const GetOrderDetails = async (req: Request, res: Response, next: NextFunction) => {

  const orderId = req.params.id;
  
  if(orderId){

      const order = await Order.findById(orderId).populate('items.food');

      if(order != null){
          return res.status(200).json(order);
      }
  }

  return res.json({ message: 'Order Not found'});
}

export const ProcessOrder = async (req: Request, res: Response, next: NextFunction) => {

  const orderId = req.params.id;

  const { status, remarks, time } = req.body;

  
  if(orderId){

      const order = await Order.findById(orderId).populate('items.food');
   if (order){
      order.orderStatus = status;
      order.remarks = remarks;
      if(time){
          order.readyTime = time;
      }

      const orderResult = await order.save();

      if(orderResult != null){
          return res.status(200).json(orderResult);
      }
    }
  }

  return res.json({ message: 'Unable to process order'});
}



export const EditOffer = async (req: Request, res: Response, next: NextFunction) => {


  const user = req.user;
  const offerId = req.params.id;

  if(user){
      const { title, description, offerType, offerAmount, pincode,
      promocode, promoType, startValidity, endValidity, bank, bins, minValue, isActive } = <CreateOfferInputs>req.body;

      const currentOffer = await Offer.findById(offerId);

      if(currentOffer){

          const vendor = await findVandor(user._id);

          if(vendor){
         
              currentOffer.title = title,
              currentOffer.description = description,
              currentOffer.offerType = offerType,
              currentOffer.offerAmount = offerAmount,
              currentOffer.pincode = pincode,
              currentOffer.promoType = promoType,
              currentOffer.startValidity = startValidity,
              currentOffer.endValidity = endValidity,
              currentOffer.bank = bank,
              currentOffer.isActive = isActive,
              currentOffer.minValue = minValue;

              const result = await currentOffer.save();

              return res.status(200).json(result);
          }
          
      }

  }

  return res.json({ message: 'Unable to add Offer!'});    

}


export const GetOffers = async (req: Request, res: Response, next: NextFunction) => {


  const user = req.user;

  if(user){
      let currentOffer = Array();

      const offers = await Offer.find().populate('vendors');

      if(offers){


          offers.map(item => {

              if(item.vendors){
                  item.vendors.map(vendor => {
                      if(vendor._id.toString() === user._id){
                          currentOffer.push(item);
                      }
                  })
              }

              if(item.offerType === "GENERIC"){
                  currentOffer.push(item)
              }

          })

      }

      return res.status(200).json(currentOffer);

  }

  return res.json({ message: 'Offers Not available'});
}


export const AddOffer = async (req: Request, res: Response, next: NextFunction) => {


  const user = req.user;

  if(user){
      const { title, description, offerType, offerAmount, pincode,
      promocode, promoType, startValidity, endValidity, bank, bins, minValue, isActive } = <CreateOfferInputs>req.body;

      const vendor = await findVandor(user._id);

      if(vendor){
        console.log("ven",vendor);
          const offer = await Offer.create({
              title,
              description,
              offerType,
              offerAmount,
              pincode,
              promoType,
              startValidity,
              endValidity,
              bank,
              isActive,
              minValue,
              vendors:[vendor]
          })

          console.log(offer);

          return res.status(200).json(offer);

      }

  }

  return res.json({ message: 'Unable to add Offer!'});

  

}

