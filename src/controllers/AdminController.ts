import { Request, Response, NextFunction } from "express";
import { CreateVandorInput } from "../dto";
import { Vandor } from "../models";
import { GeneratePassword, GenerateSalt } from "../utility";

export const findVandor = async( id: string, email?:string) =>{
    if (email) {
        return await Vandor.findOne({email:email});
    } else {
        return await Vandor.findById(id);
    }

} 
// create a Vandor contains name , email , pincode , [foodType], password, salt, rating,service available,images, [food]
// 
export const CreateVandor = async (req: Request, res: Response, next: NextFunction) => {
    const { name, owner, foodType, pincode, address, phone, email, password } = <CreateVandorInput>req.body;

    const existingVandor = await findVandor('',email);
    console.log("exu",existingVandor);
    if (existingVandor != null) {
        return res.json({
            message: "User Already Exist"
        });
    }

    const salt = await GenerateSalt();
    const userPassword = await GeneratePassword(password, salt);

    const createVandor = await Vandor.create({
        name: name,
        address: address,
        pincode: pincode,
        owner:owner,
        foodType: foodType,
        salt: salt,
        email: email,
        password: userPassword,
        phone: phone,
        rating: 0,
        serviceAvailable: false,
        coverImages: [],
        foods:[]
    });

    res.json(createVandor);
}

// get all Vandors
export const getVandors = async (req: Request, res: Response, next: NextFunction) => {

const vandors = await Vandor.find();

if (vandors !==null) {
 return res.json(vandors);   
}

return res.json({message:"No Vandors available"});

}

// get VandorbY ID
export const getVandorById = async (req: Request, res: Response, next: NextFunction) => {
  
 const vandor = await findVandor(req.params.id);
 console.log(vandor);
 if (vandor!=null) {
    return res.json(vandor)
 }
 return res.json({message:'No vandor found'});
}