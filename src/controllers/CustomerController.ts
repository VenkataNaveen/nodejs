import { plainToClass } from 'class-transformer';
import { validate } from 'class-validator';
import express, { Request, Response, NextFunction } from 'express';
import { CartItem, CreateCustomerInput, CreateFoodInput, EditCustomerProfileInput, OrderInputs, UserLoginInput } from '../dto';
import {Customer, Food, Order, Transaction, Vandor} from '../models';
// import { Offer } from '../models/Offer';
// import { Order } from '../models/Order';
// import { Transaction } from '../models/Transaction';
import { GenerateOtp, GeneratePassword, GenerateSalt, GenerateSignature, onRequestOTP, ValidatePassword } from '../utility';
import { Offer } from '../models/offer';

export const CustomerSignUp = async (req: Request, res: Response, next: NextFunction) => {

    const customerInputs = plainToClass(CreateCustomerInput, req.body);

    const validationError = await validate(customerInputs, {validationError: { target: true}})

    if(validationError.length > 0){
        return res.status(400).json(validationError);
    }

    const { email, phone, password } = customerInputs;

    const salt = await GenerateSalt();
    const userPassword = await GeneratePassword(password, salt);

    const { otp, expiry } = GenerateOtp();

    const existingCustomer =  await Customer.findOne({ email: email});
    console.log(existingCustomer);
    if(existingCustomer !== null){
        return res.status(400).json({message: 'Email already exist!'});
    }

    const result = await Customer.create({
        email: email,
        password: userPassword,
        salt: salt,
        phone: phone,
        otp: otp,
        otp_expiry: expiry,
        firstName: '',
        lastName: '',
        address: '',
        verified: false,
        lat: 0,
        lng: 0,
        orders: []
    })

    if(result){
        // send OTP to customer
        await onRequestOTP(otp, phone);
        
        //Generate the Signature
        const signature = await GenerateSignature({
            _id: result._id,
            email: result.email,
            verified: result.verified
        })
        // Send the result
        return res.status(201).json({signature, verified: result.verified, email: result.email})

    }

    return res.status(400).json({ msg: 'Error while creating user'});


}

export const CustomerLogin = async (req: Request, res: Response, next: NextFunction) => {

    
    const customerInputs = plainToClass(UserLoginInput, req.body);

    const validationError = await validate(customerInputs, {validationError: { target: true}})

    if(validationError.length > 0){
        return res.status(400).json(validationError);
    }

    const { email, password } = customerInputs;
    const customer = await Customer.findOne({ email: email});
    if(customer){
        const validation = await ValidatePassword(password, customer.password, customer.salt);
        
        if(validation){

            const signature = GenerateSignature({
                _id: customer._id,
                email: customer.email,
                verified: customer.verified
            })

            return res.status(200).json({
                signature,
                email: customer.email,
                verified: customer.verified
            })
        }
    }

    return res.json({ msg: 'Error With Signup'});

}

export const CustomerVerify = async (req: Request, res: Response, next: NextFunction) => {


    const { otp } = req.body;
    const customer = req.user;

    if(customer){
        const profile = await Customer.findById(customer._id);
        if(profile){
            if(profile.otp === parseInt(otp) && profile.otp_expiry >= new Date()){
                profile.verified = true;

                const updatedCustomerResponse = await profile.save();

                const signature = GenerateSignature({
                    _id: updatedCustomerResponse._id,
                    email: updatedCustomerResponse.email,
                    verified: updatedCustomerResponse.verified
                })

                return res.status(200).json({
                    signature,
                    email: updatedCustomerResponse.email,
                    verified: updatedCustomerResponse.verified
                })
            }
             return res.status(400).json({ msg: 'either otp is invalid or it got expired'});
            
        }

    }

    return res.status(400).json({ msg: 'Unable to verify Customer'});
}

export const RequestOtp = async (req: Request, res: Response, next: NextFunction) => {

    const customer = req.user;

    if(customer){

        const profile = await Customer.findById(customer._id);

        if(profile){
            const { otp, expiry } = GenerateOtp();
            profile.otp = otp;
            profile.otp_expiry = expiry;

            await profile.save();
            const sendCode = await onRequestOTP(otp, profile.phone);

            if (!sendCode) {
                return res.status(400).json({ message: 'Failed to verify your phone number' })
            }

            return res.status(200).json({ message: 'OTP sent to your registered Mobile Number!'})

        }
    }

    return res.status(400).json({ msg: 'Error with Requesting OTP'});
}

export const GetCustomerProfile = async (req: Request, res: Response, next: NextFunction) => {

    const customer = req.user;
 
    if(customer){
        
        const profile =  await Customer.findById(customer._id);
        
        if(profile){
             
            return res.status(201).json(profile);
        }

    }
    return res.status(400).json({ msg: 'Error while Fetching Profile'});

}

export const EditCustomerProfile = async (req: Request, res: Response, next: NextFunction) => {


    const customer = req.user;

    const customerInputs = plainToClass(EditCustomerProfileInput, req.body);

    const validationError = await validate(customerInputs, {validationError: { target: true}})

    if(validationError.length > 0){
        return res.status(400).json(validationError);
    }

    const { firstName, lastName, address } = customerInputs;

    if(customer){
        
        const profile =  await Customer.findById(customer._id);
        
        if(profile){
            profile.firstName = firstName;
            profile.lastName = lastName;
            profile.address = address;
            const result = await profile.save()
            
            return res.status(201).json(result);
        }

    }
    return res.status(400).json({ msg: 'Error while Updating Profile'});

}


const validateTransaction = async(txnId: string) => {
    
    const currentTransaction = await Transaction.findById(txnId);

    if(currentTransaction){
        if(currentTransaction.status.toLowerCase() !== 'failed'){
            return {status: true, currentTransaction};
        }
        return {status: false, currentTransaction};
    }
    return {status: false, currentTransaction};
}


export const CreateOrder = async (req: Request, res: Response, next: NextFunction) => {


    const customer = req.user;

     const { txnId, amount, items } = <OrderInputs>req.body;

    
    if(customer){

        const { status, currentTransaction } =  await validateTransaction(txnId);

        if(!status){
            return res.status(404).json({ message: 'Error while Creating Order!'})
        }

        const profile = await Customer.findById(customer._id);


        const orderId = `${Math.floor(Math.random() * 89999)+ 1000}`;

        const cart = <[CartItem]>req.body.items;

        let cartItems = Array();

        let netAmount = 0.0;

        let vendorId="";

        const foods = await Food.find().where('_id').in(cart.map(item => item._id)).exec();
   
        foods.map(food => {
            cart.map(({ _id, unit}) => {
                if(food._id == _id){
                    vendorId = food.vendorId;
                    netAmount += (food.price * unit);
                    cartItems.push({ food, unit})
                }
            })
        })

        if(cartItems && profile && currentTransaction){

            const currentOrder = await Order.create({
                orderId: orderId,
                vendorId: vendorId,
                items: cartItems,
                totalAmount: netAmount,
                paidAmount: amount,
                orderDate: new Date(),
                orderStatus: 'Waiting',
                remarks: '',
                deliveryId: '',
                readyTime: 45
            })

            profile.cart = [] as any;
            profile?.orders.push(currentOrder);
 

            currentTransaction.vendorId = vendorId;
            currentTransaction.orderId = orderId;
            currentTransaction.status = 'CONFIRMED'
            
            await currentTransaction.save();

          //  await assignOrderForDelivery(currentOrder._id, vendorId);

            const profileResponse =  await profile.save();

            return res.status(200).json(profileResponse);

        }

    }

    return res.status(400).json({ msg: 'Error while Creating Order'});
}


export const getOrderById=async (req: Request,res: Response,next: NextFunction) =>{
    const orderId= req.params.id;
    if(orderId) {
        const order = await Order.findById(orderId).populate('items.food');
        if(order) {
            return res.status(200).json(order);
        }
    }
    
}
export const getOrders=async (req: Request,res: Response,next: NextFunction) =>{
    const customer = req.user;
    if(customer){
        const profile = await Customer.findById(customer._id).populate('orders');
        
        return res.status(200).json(profile?.orders);

    }

}

export const addToCart = async (req: Request,res: Response,next: NextFunction) =>{

    const customer = req.user;
    const {_id,unit} =<CartItem>req.body;
    const food = await Food.findById(_id);
    if (food) {
      
        const profile = await Customer.findById(customer?._id);
        if (profile) {
            if (profile.cart.length){
                let existingItem = profile.cart.filter((item)=> item.food._id == _id);
                let cartItem = profile.cart;
                if (existingItem.length){
                let index= cartItem.indexOf(existingItem[0]);
                if (unit>0){
                  profile.cart[index] ={food,unit};
                } else {
                    profile.cart.splice(index,1);
                }
                } else {
                    profile.cart.push({food,unit}); 
                }
            } else {
                profile.cart.push({food,unit});
            }
            // profile.cart= [] as any;
         const cartItems=  await  profile.save()

        return res.status(200).json(cartItems.cart);
        }
    } else {
        return res.status(400).json({message: "no food Available"})
    }

}

export const getCart = async (req: Request,res: Response,next: NextFunction) =>{
    const customer = req.user;
    const profile = await Customer.findById(customer?._id).populate("cart.food");
    if(profile) {
    return res.status(200).json(profile.cart);
    }
}

export const deleteCart = async (req: Request,res: Response,next: NextFunction) =>{
    const customer = req.user;
    const profile = await Customer.findById(customer?._id);
    if(profile) {
        profile.cart =[] as any;
        const cartDetails = await profile.save();
     return res.status(200).json(cartDetails.cart);
    }

}


export const VerifyOffer = async (req: Request, res: Response, next: NextFunction) => {

    const offerId = req.params.id;
    const customer = req.user;
    
    if(customer){

        const appliedOffer = await Offer.findById(offerId);
        
        if(appliedOffer){
            if(appliedOffer.isActive){
                return res.status(200).json({ message: 'Offer is Valid', offer: appliedOffer});
            }
        }

    }

    return res.status(400).json({ msg: 'Offer is Not Valid'});
}


export const CreatePayment = async (req: Request, res: Response, next: NextFunction) => {
    try {
    const customer = req.user;

    const { amount, paymentMode, offerId} = req.body;

    let payableAmount = Number(amount);

    if(offerId){

        const appliedOffer = await Offer.findById(offerId);

        if(appliedOffer && appliedOffer?.isActive){
            payableAmount = (payableAmount - appliedOffer.offerAmount);
        }
    }
    // perform payment gateway charge api

    // create record on transaction
    const transaction = await Transaction.create({
        customer: customer?._id,
        vendorId: '',
        orderId: '',
        orderValue: payableAmount,
        offerUsed: offerId || 'NA',
        status: 'OPEN',
        paymentMode: paymentMode,
        paymentResponse: 'Payment is cash on Delivery'
    })


    //return transaction
    return res.status(200).json(transaction);
}  catch(err) {
   res.status(400).json({
    "message":err
   })
}

}